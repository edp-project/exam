#!/usr/bin/env python2

import math
import sys
import numpy as np
import matplotlib.pyplot as plt


def u0(x):
    return math.sin(2*math.pi*x)
def nonRegular(x):
    if x >= 0.5 and x <= 0.8 :
        return 1
    return 0

def initial_scheme(J,a,c,T,show,useRegular):
    dx   = 1.0 / J
    dt   = (c*dx)/math.fabs(a)
    ratio= dt / dx
    
    uL   = np.zeros(J)
    uR   = np.zeros(J)
    u    = np.zeros(J)
    
    if useRegular :
        for j in range(J):
            u[j]  =  u0(j*dx)
    else:
        for j in range(J):
            u[j]  =  nonRegular(j*dx)

    for j in range(J-1):
        uL[j] =  u[j+1]
    for j in range(1,J):
        uR[j] = u[j-1]
    
    t = 0
    X    =    np.linspace(0,1,J,endpoint=True)
    niter = 0
    while t < T :
        t += dt
        # uL loop
        for j in range(1,J):
            uL[j] = -a*(u[j] - u[j-1]) - a*0.5*(1 - a*ratio)*(u[j-1] - u[j])
        # uR loop
        for j in range(J-1):
            uR[j] = -0.5*a*(1 - a*ratio)*(u[j+1] - u[j])
        # introduce here the value of uR[0] , uL[J-1]
        uL[0] = -a*(u[0] - u[J-1]) - a*0.5*(1 - a*ratio)*(u[J-1] - u[0])
        uR[0] = -0.5*a*(1 - a*ratio)*(u[0] - u[J-1])
    
        for j in range(J):
            u[j] = u[j] - ratio*(uR[j] - uL[j])
    
        
        niter = niter + 1
    
        if (niter%10 == 0) and show:
            plt.hold(False)
            plt.plot(X,u,"b:.")
            plt.grid()
            plt.pause(dt)
            plt.hold(True)
            print("iteration n = ", niter, "temps t = ",t)
    
    if show:
        print("End of iteration")
        plt.plot(X,u)
        plt.show()
    return u

def __main__():
    print ("Question 1")
    J    = 100
    a    = 1.0
    c    = 1
    T    = 1
    initial_scheme(J,a,c,T,True,True)
    print("Question 2")
    print("Graph of error")

    error = np.zeros(100)
    for j in range(10,109):
        u = initial_scheme(j,a,c,T,False,True)
        dx = 1.0/j
        dt = (c*dx)/math.fabs(a)
        for i in range(j-1):
            error[j-10] += dx*math.fabs(math.fabs(u[i+1] - u0((i+1)*dx)) - math.fabs(u[i] - u0(i*dx)))

    X    =    np.linspace(10,109,100,endpoint=True)
    plt.plot(X,error,"b:.")
    plt.show()
    print("Question 3")
    initial_scheme(J,a,c,T,True,False)
__main__()
